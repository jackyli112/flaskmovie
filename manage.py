#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:47
# 文件名称：manage.PY
#项目入口文件
from app import app

if __name__ == '__main__':
    app.run()