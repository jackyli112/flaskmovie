#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:48
# 文件名称：__init__.py.PY
# from flask import Flask, render_template
# # from app.home import home as home_blueprint
# # from app.admin import admin as admin_blueprint
# #
# # 创建APP对象
# app = Flask(__name__)
# app.debug = True
# #
# # # 注册蓝图
# # app.register_blueprint(home_blueprint)
# # app.register_blueprint(admin_blueprint, url_prefix='/admin')
#
#
# # 404
# @app.errorhandler(404)
# def page_not_found(error):
#     return render_template('home/404.html'), 404
