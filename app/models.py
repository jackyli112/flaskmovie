#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:48
# 文件名称：modules.PY
from flask import Flask
from app import db
from datetime import datetime
import pymysql
# 密码加密工具
from werkzeug.security import generate_password_hash



app = Flask(__name__)
# app配置
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:123456@127.0.0.1:3306/movie'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True


# 会员模型
class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    # 昵称
    name = db.Column(db.String(100), unique = True)
    pwd = db.Column(db.String(100), nullable = False)
    email = db.Column(db.String(100), unique = True)
    phone = db.Column(db.String(11), unique = True)
    # 简介
    info = db.Column(db.Text)
    # 头像
    face = db.Column(db.String(255), unique = True)
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)
    # 唯一标识符
    uuid = db.Column(db.String(255), unique = True)
    # 会员日志外键关系关联
    userlogs = db.relationship('UserLog', backref = "users")
    comments = db.relationship('Comment', backref = "users")
    moviecols = db.relationship('Moviecol', backref = "users")

    def __repr__(self):
        return '<User %r>' % self.name

    def check_pwd(self, pwd):
        from werkzeug.security import check_password_hash
        return check_password_hash(self.pwd, pwd)


# 会员登录日志
class UserLog(db.Model):
    __tablename__ = "userlog"
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    ip = db.Column(db.String(100))
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return '<UserLog %r>' % self.id


# 标签
class Tag(db.Model):
    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key = True)
    # 标题
    name = db.Column(db.String(100), unique = True)
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)
    movies = db.relationship('Movie', backref = 'tag')  # 返回这个标签的相关电影的列表

    def __repr__(self):
        return "<Tag %r>" % self.name


'''
relationship & backref
通过db.relationship()，Role 模型有了一个可以获得对应角色所有用户的属性users。默认是列表形式，
lazy='dynamic'时返回的是一个 query 对象。即relationship提供了 Role 对 User 的访问。
而backref正好相反，提供了 User 对 Role 的访问。
不妨设一个 Role 实例为 user_role，一个 User 实例为 u。relationship 使 user_role.users 
可以访问所有符合角色的用户，而 backref 使 u.role 可以获得用户对应的角色。
movie.tag获取该电影对应的标签
'''


# 电影
class Movie(db.Model):
    __tablename__ = "movie"
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255), index = True, unique = True)  # 标题
    url = db.Column(db.String(255), unique = True)  # 电影地址
    info = db.Column(db.Text)  # 简介
    logo = db.Column(db.String(255), unique = True)  # 封面
    star = db.Column(db.SmallInteger)  # 星级
    playnum = db.Column(db.BigInteger)  # 播放量
    commentnum = db.Column(db.BigInteger)  # 评论量
    # 电影所属标签
    tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'))
    # 上映地区
    area = db.Column(db.String(255))
    # 上映时间
    release_time = db.Column(db.Date)
    # 电影长度
    length = db.Column(db.String(100))
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)
    comments = db.relationship('Comment', backref = "movie")  # 电影评论外键关系关联
    moviecols = db.relationship('Moviecol', backref = "movie")  # 电影评论外键关系关联

    def __repr__(self):
        return "<Movie %r>" % self.title


# 电影预告模型
class Preview(db.Model):
    __tablename__ = "preview"
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255), index = True, unique = True)  # 标题
    logo = db.Column(db.String(255), unique = True)  # 封面
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return "<Preview %r>" % self.title


# 评论模型
class Comment(db.Model):
    __tablename__ = "comment"
    id = db.Column(db.Integer, primary_key = True)
    # 评论内容
    content = db.Column(db.Text)
    # 所属电影
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    # 所属用户
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return "<Comment %r>" % self.id


# 电影收藏
class Moviecol(db.Model):
    __tablename__ = "moviecol"
    id = db.Column(db.Integer, primary_key = True)
    # 收藏
    content = db.Column(db.Text)
    # 所属电影
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    # 所属用户
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return "<Moviecol %r>" % self.id


# 权限
class Auth(db.Model):
    __tablename__ = "auth"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100), unique = True)  # 权限名称
    url = db.Column(db.String(255), unique = True)  # 权限url地址
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)  # 权限添加时间

    def __repr__(self):
        return "<Auth %r>" % self.name


# 角色
class Role(db.Model):
    __tablename__ = "role"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100), unique = True)  # 角色名称
    auths = db.Column(db.String(600))  # 权限列表
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)  # 添加时间
    admins = db.relationship('Admin', backref = "role")  # admin.role

    def __repr__(self):
        return "<Role %r>" % self.name


# 管理员
class Admin(db.Model):
    __tablename__ = "admin"
    id = db.Column(db.Integer, primary_key = True)
    # 管理员账号
    name = db.Column(db.String(100), unique = True)
    # 管理员密码
    pwd = db.Column(db.String(100), nullable = False)
    is_super = db.Column(db.SmallInteger)  # 是否为超级管理 0为超级管理员
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))  # 关联角色表
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)  # 添加时间
    adminlogs = db.relationship('AdminLog', backref = "admin")  # adminlog.admin反查到日志的管理员
    optlogs = db.relationship('OptLog', backref = "admin")

    def __repr__(self):
        return "<Admin %r>" % self.name

    # 验证哈希密码
    def check_pwd(self, pwd):
        from werkzeug.security import check_password_hash
        return check_password_hash(self.pwd, pwd)  # 如果相同则为True


# 管理员登录日志
class AdminLog(db.Model):
    __tablename__ = "adminlog"
    id = db.Column(db.Integer, primary_key = True)
    admin_id = db.Column(db.Integer, db.ForeignKey('admin.id'))
    ip = db.Column(db.String(100))
    # 登录时间
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return '<AdminLog %r>' % self.id


# 管理员操作日志
class OptLog(db.Model):
    __tablename__ = "optlog"
    id = db.Column(db.Integer, primary_key = True)
    admin_id = db.Column(db.Integer, db.ForeignKey('admin.id'))
    ip = db.Column(db.String(100))
    reason = db.Column(db.String(600))
    # 登录时间
    addtime = db.Column(db.DateTime, index = True, default = datetime.now)

    def __repr__(self):
        return '<OptLog %r>' % self.id


if __name__ == '__main__':
    # db.drop_all()
    # db.create_all()
    # 测试数据
    # role = Role(
    #     name ="超级管理员",
    #     auths = ""
    # )
    # db.session.add(role)
    # db.session.commit()
    admin = Admin(
        name = "imoocmovie",
        pwd = generate_password_hash("liwuji112"),
        is_super = 0,
        role_id = 1
    )
    db.session.add(admin)
    db.session.commit()
