#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:48
# 文件名称：__init__.py.PY
from flask import  Flask,render_template
from flask_sqlalchemy import SQLAlchemy
import pymysql
import os

#创建APP对象
app = Flask(__name__)
app.debug  = True
#app配置
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:123456@127.0.0.1:3306/movie'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#表单配置
app.config['SECRET_KEY']='a random key'
#上传目录
app.config['UP_DIR_MOVIE'] =os.path.join(os.path.abspath(os.path.dirname(__file__)),'static/uploads/movie/')
app.config['UP_DIR_USER'] =os.path.join(os.path.abspath(os.path.dirname(__file__)),'static/uploads/users/')
app.config['UP_DIR_USERS'] =os.path.join(os.path.abspath(os.path.dirname(__file__)),'static/uploads/fc/')
app.config['UP_DIR_PREVIEW'] =os.path.join(os.path.abspath(os.path.dirname(__file__)),'static/uploads/preview/')
#实例化数据库对象
db = SQLAlchemy(app)

from app.home import home as home_blueprint
from app.admin import admin as admin_blueprint


#注册蓝图
app.register_blueprint(home_blueprint)
app.register_blueprint(admin_blueprint,url_prefix = '/admin')

# 404
@app.errorhandler(404)
def page_not_found(error):
    return render_template('home/404.html'), 404