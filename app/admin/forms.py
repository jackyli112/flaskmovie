#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:50
# 文件名称：forms.PY
# 导入表单
from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, PasswordField, FileField, TextAreaField, \
    SelectField,SelectMultipleField
from wtforms.validators import DataRequired, ValidationError,EqualTo
from app.models import Admin, Tag,Auth,Role
auth_list = Auth.query.all()
role_list = Role.query.all()

# 登录表单
class LoginForm(FlaskForm):
    '''管理员登录的表单'''
    account = StringField(
        label = "账号",
        # 验证器
        validators = [
            DataRequired('请输入账号！')
        ],
        description = "账号",
        # 附加选项
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入账号！",
            # 必须的选项
            # "required": "required"
        }
    )
    pwd = PasswordField(
        label = "密码",
        # 验证器
        validators = [
            DataRequired('请输入账号！')
        ],
        description = "密码",
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入密码！",
            # 必须的选项
            # "required": "required"
        }
    )
    submit = SubmitField(
        "登录",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary btn-block btn-flat",
            # "required": "required"
        }
    )

    # 验证账号是否正确
    def validate_account(self, field):
        account = field.data
        # 在admin数据库中查询和account相匹配的记录数
        admin = Admin.query.filter_by(name = account).count()
        if admin == 0:
            # 抛出验证错误
            raise ValidationError("账号不存在！")


class TagForm(FlaskForm):
    name = StringField(
        label = "名称",
        validators = [
            DataRequired('请输入标签!'),
        ],
        description = '标签',
        render_kw = {
            "class": "form-control",
            "id": "input_name",
            "placeholder": "请输入标签名称！"
        }
    )

    submit = SubmitField(
        "提交",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary",
            # "required": "required"
        }
    )


class MovieForm(FlaskForm):
    title = StringField(
        label = '电影名称',
        validators = [
            DataRequired('请输入电影名称！')
        ],
        description = '片名',
        render_kw = {
            "class": "form-control",
            "id": "input_title",
            "placeholder": "请输入电影名称！"
        }
    )

    url = FileField(
        label = '文件',
        validators = [
            DataRequired('请上传文件！')
        ],
        description = '文件'
    )

    info = TextAreaField(
        label = '简介',
        validators = [
            DataRequired('请输入简介！')
        ],
        description = '简介',
        render_kw = {
            "class": "form-control",
            "id": "input_info",
            "rows": 10
        }
    )

    logo = FileField(
        label = '封面',
        validators = [
            DataRequired('请上传封面！')
        ],
        description = '封面'
    )
    star = SelectField(
        label = '星级',
        validators = [
            DataRequired('请选择星级！')
        ],
        # 数据类型
        coerce = int,
        choices = [(1, "1星"), (2, "2星"), (3, "3星"), (4, "4星"), (5, "5星")],
        description = '星级',
        render_kw = {
            "class": "form-control",
            "id": "input_star"
        }
    )
    tags = Tag.query.all()
    tag_id = SelectField(
        label = '标签',
        validators = [
            DataRequired('请选择标签！')
        ],
        # 数据类型
        coerce = int,
        choices = [(v.id, v.name) for v in tags],
        description = '标签',
        render_kw = {
            "class": "form-control",
            "id": "input_tag_id"
        }
    )

    area = StringField(
        label = '上映地区',
        validators = [
            DataRequired('请输入上映地区！')
        ],
        description = '上映地区',
        render_kw = {
            "class": "form-control",
            "id": "input_area",
            "placeholder": "请输入地区！"
        }
    )

    length = StringField(
        label = '片长',
        validators = [
            DataRequired('请输入片长！')
        ],
        description = '片长',
        render_kw = {
            "class": "form-control",
            "id": "input_length",
            "placeholder": "请输入片长！"
        }
    )
    release_time = StringField(
        label = '上映时间',
        validators = [
            DataRequired('请选择上映时间！')
        ],
        description = '上映时间',
        render_kw = {
            "class": "form-control",
            "id": "input_release_time",
            "placeholder": "请选择上映时间！"
        }
    )
    submit = SubmitField(
        "提交",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary",
            # "required": "required"
        }
    )

class PreviewForm(FlaskForm):
    title = StringField(
        label = '预告标题',
        validators = [
          DataRequired('请输入预告标题')
        ],
        description = '预告标题',
        render_kw = {
            "class": "form-control",
            "id": "input_title",
            "placeholder": "请输入预告标题！"
        }
    )
    logo = FileField(
        label = '预告封面',
        validators = [
            DataRequired('请上传封面！')
        ],
        description = '预告封面'
    )
    submit = SubmitField(
        "提交",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary"
            # "required": "required"
        }
    )


class PwdForm(FlaskForm):
    old_pwd = PasswordField(
        label = '旧密码',
        validators = [
            DataRequired('请输入旧密码')
        ],
        description = '旧密码',
        render_kw = {
            "class":"form-control",
            "placeholder":"请输入旧密码"
        }
    )
    new_pwd = PasswordField(
        label ='新密码',
        validators = [
            DataRequired('请输入新密码！')
        ],
        description = '新密码',
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入新密码"
        }
    )
    submit = SubmitField(
        "提交",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary"
            # "required": "required"
        }
    )

    def validate_old_pwd(self,field):
        from flask import session
        pwd = field.data
        name = session.get('admin')
        admin = Admin.query.filter_by(
            name = name
        ).first()
        if not admin.check_pwd(pwd):
            raise ValidationError('旧密码错误！')



class AuthForm(FlaskForm):
    name = StringField(
        label = "权限名称",
        validators = [
            DataRequired('请输入权限名称')
        ],
        description = "权限名称",
        render_kw = {
            "class": "form-control",
            "id": "input_name",
            "placeholder": "请输入权限名称！"
        }
    )


    url = StringField(
        label = '权限地址',
        validators = [
            DataRequired('请输入权限地址'),
        ],
        description = '权限地址',
        render_kw = {
            "class": "form-control",
            "id": "input_url",
            "placeholder": "请输入权限地址！"
        }
    )

    submit = SubmitField(
        '提交',
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-primary",
        # "required": "required"
        }
    )



class RoleForm(FlaskForm):
    name = StringField(
        label = '角色名称',
        validators = [
            DataRequired('请输入角色名称')
        ],
        description = '角色名称',
        render_kw = {
            "class": "form-control",
            "id": "input_name",
            "placeholder": "请输入角色名称！"
        }
    )
    auths = SelectMultipleField(
        label = '权限列表',
        validators = [
            DataRequired('请选择权限')
        ],
        coerce = int,
        choices = [(v.id, v.name) for v in auth_list ],
        description = '权限列表',
        render_kw = {
            "class": "form-control",
        }
    )

    submit = SubmitField(
        '提交',
        render_kw = {
            "class": "btn btn-primary",
        }
    )


class AdminForm(FlaskForm):
    name = StringField(
        label = "管理员名称",
        validators = [
            DataRequired('请输入管理员名称'),
        ],
        description = '管理员名称',
        render_kw = {
            "class": "form-control",
            "id": "input_name",
            "placeholder": "请输入管理员名称！"
        }
    )

    pwd = PasswordField(
        label = '密码',
        validators = [
            DataRequired('请输入密码'),
        ],
        description = '密码',
        render_kw = {
            "class": "form-control",
            "id": "input_pwd",
            "placeholder": "请输入管理员密码！"
        }
    )
    re_pwd = PasswordField(
        label = '管理员重复密码',
        validators = [
            DataRequired('请输入管理员重复密码'),
            EqualTo('pwd',message = "两次密码不一致")
        ],
        description = '管理员重复密码',
        render_kw = {
            "class": "form-control",
            "id": "input_re_pwd",
            "placeholder": "请输入管理员重复密码！"
        }
    )
    role_id = SelectField(
        label = "所属角色",

        validators = [
            DataRequired('请选择角色！'),
        ],
        # 数据类型
        coerce = int,
        choices = [(v.id, v.name) for v in role_list],
        description = '所属角色',
        render_kw = {
            "class": "form-control",
            "id": "input_role_id"
        }
    )

    submit = SubmitField(
        '提交',
        render_kw = {
            "class": "btn btn-primary",
        }
    )