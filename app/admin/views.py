#!/usr/bin/env python
# encoding: utf-8
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:50
# 文件名称：views.PY
# 调用蓝图
import os
import uuid
import datetime
from app import db, app
from . import admin
from flask import Flask, render_template, redirect, url_for, flash, session, request,abort
from app.admin.forms import LoginForm, TagForm, MovieForm, PreviewForm, PwdForm, AuthForm, RoleForm, AdminForm
from app.models import Admin, Tag, Movie, Preview, User, Comment, Moviecol, OptLog, AdminLog, UserLog, Auth, Role, Admin
from functools import wraps  # 装饰器访问限制
from werkzeug.utils import secure_filename  # 引入安全文件


# 上下文处理器
@admin.context_processor
# 模板内的其余项
def tpl_extra():
    data = dict(
        # 当前在线时间
        online_time = datetime.datetime.now().strftime("%Y-%m-%d")
    )
    return data


# 访问限制处理器
def admin_login_req(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # 如果session中没有admin
        if 'admin' not in session:
            return redirect(url_for('admin.login', next = request.url))
        return f(*args, **kwargs)

    return decorated_function


# 权限访问控制处理器
def admin_auth(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # 查询我们的权限
        admin = Admin.query.join(
            Role
        ).filter(
            Role.id == Admin.role_id,
            Admin.id == session['admin_id']
        ).first()
        #获取auths
        auths = admin.role.auths
        print(auths)
        auths = list(map(lambda v : int(v),auths.split(",")))
        auth_list = Auth.query.all()
        urls = [ v.url for v in auth_list for val in auths  if val== v.id ]
        rule = request.url_rule
        if str(rule) not in urls:
            abort(404)

        return f(*args, **kwargs)

    return decorated_function




# 改变文件名称
def change_filename(filename):
    fileinfo = os.path.splitext(filename)  # 分割成后缀名加前面标题
    filename = datetime.date.today().strftime("%Y-%m-%d") + str(uuid.uuid4().hex) + fileinfo[-1]
    return filename


@admin.route('/')
@admin_login_req
def index():
    return render_template('admin/index.html')


# 后台登录界面
@admin.route('/login/', methods = ["GET", "POST"])
def login():
    # 实例化表单
    form = LoginForm()
    # 表单处理
    # 如果表单验证成功
    if form.validate_on_submit():
        data = form.data
        admin = Admin.query.filter_by(name = data['account']).first()  # 验证账号
        # 当admin.check_pwd(data['pwd'])为true if 结果就为false 就不会显示密码错误
        if not admin.check_pwd(data['pwd']):
            flash('密码错误！')
            return redirect(url_for('admin.login'))
        session['admin'] = data['account']
        session['admin_id'] = admin.id
        adminlog = AdminLog(
            admin_id = admin.id,
            ip = request.remote_addr
        )
        db.session.add(adminlog)
        db.session.commit()
        # request.args.get('next')表示关闭页面之前要访问的页面
        return redirect(request.args.get('next') or url_for('admin.index'))
    return render_template("admin/login.html", form = form)


# 后台退出界面
@admin.route('/logout/')
@admin_login_req
def logout():
    session.pop('admin', None)
    session.pop('admin_id', None)
    return redirect(url_for('admin.login'))


# 修改密码
@admin.route('/pwd/', methods = ['GET', 'POST'])
@admin_login_req
def pwd():
    form = PwdForm()
    if form.validate_on_submit():
        data = form.data
        admin = Admin.query.filter_by(name = session['admin']).first()
        from werkzeug.security import generate_password_hash
        admin.pwd = generate_password_hash(data['new_pwd'])
        db.session.add(admin)
        db.session.commit()
        flash('修改密码成功,请重新登录', 'ok')
        return redirect(url_for('admin.logout'))
    return render_template("admin/pwd.html", form = form)


# 添加管理员
@admin.route('/admin_add/', methods = ['GET', 'POST'])
@admin_login_req
def admin_add():
    form = AdminForm()
    from werkzeug.security import generate_password_hash
    if form.validate_on_submit():
        data = form.data
        admin = Admin(
            name = data.get('name'),
            pwd = generate_password_hash(data.get('pwd')),
            role_id = data.get('role_id'),
            is_super = 1,
        )
        db.session.add(admin)
        db.session.commit()
        flash('管理员添加成功', 'ok')
    return render_template("admin/admin_add.html", form = form)


@admin.route('/admin_list/<int:page>', methods = ['GET'])
@admin_login_req
def admin_list(page=None):
    if page == None:
        page = 1
    page_data = Admin.query.join(
        Role).filter(
        Admin.role_id == Role.id
    ).order_by(
        Admin.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/admin_list.html", page_data = page_data)


@admin.route('/adminloginlog_list/<int:page>', methods = ['GET'])
@admin_login_req

def adminloginlog_list(page=None):
    if page == None:
        page = 1
    page_data = AdminLog.query.join(
        Admin
    ).filter(
        AdminLog.admin_id == Admin.id
    ).order_by(
        AdminLog.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/adminloginlog_list.html", page_data = page_data)


# 权限添加
@admin.route('/auth_add/', methods = ['GET', 'POST'])
@admin_login_req

def auth_add():
    form = AuthForm()
    if form.validate_on_submit():
        data = form.data
        auth = Auth(
            name = data.get('name'),  # {'name':'lihuafeng'}
            url = data.get('url')
        )
        db.session.add(auth)
        db.session.commit()
        flash('权限添加成功', 'ok')
    return render_template("admin/auth_add.html", form = form)


@admin.route('/auth_list/<int:page>', methods = ['GET'])
@admin_login_req

def auth_list(page=None):
    if page == None:
        page = 1
    page_data = Auth.query.order_by(
        Auth.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/auth_list.html", page_data = page_data)


# 删除权限
@admin.route('/auth_del/<int:id>', methods = ['GET'])
@admin_login_req

def auth_del(id=None):
    auth = Auth.query.get_or_404(int(id))
    db.session.delete(auth)
    db.session.commit()
    flash('删除评论成功', 'ok')
    return redirect(url_for('admin.auth_list', page = 1))


# 修改权限
@admin.route('/auth_edit/<int:id>', methods = ['GET', 'POST'])
@admin_login_req
def auth_edit(id=None):
    form = AuthForm()
    auth = Auth.query.get_or_404(int(id))
    if form.validate_on_submit():
        data = form.data
        auth.name = data.get('name')
        auth.url = data.get('url')
        db.session.add(auth)
        db.session.commit()
        flash('权限修改成功', 'ok')
        return redirect(url_for('admin.auth_edit', id = auth.id))
    return render_template('admin/auth_edit.html', form = form, auth = auth)


# 评论列表
@admin.route('/comment_list/<int:page>', methods = ['GET'])
@admin_login_req

def comment_list(page=None):
    if page == None:
        page = 1
    page_data = Comment.query.join(
        Movie
    ).join(
        User
    ).filter(
        Comment.movie_id == Movie.id,
        Comment.user_id == User.id,
    ).order_by(
        Comment.addtime.desc()
    ).paginate(page = page, per_page = 10)
    return render_template("admin/comment_list.html", page_data = page_data)


# 删除评论
@admin.route('/comment_del/<int:id>', methods = ['GET'])
@admin_login_req

def comment_del(id=None):
    comment = Comment.query.get_or_404(int(id))
    db.session.delete(comment)
    db.session.commit()
    flash('删除评论成功', 'ok')
    return redirect(url_for('admin.comment_list', page = 1))


@admin.route('/movie_add/', methods = ["GET", "POST"])
@admin_login_req

def movie_add():
    form = MovieForm()
    if form.validate_on_submit():
        data = form.data
        # 获取上传文件地址
        file_url = secure_filename(form.url.data.filename)
        file_logo = secure_filename(form.logo.data.filename)

        if not os.path.exists(app.config['UP_DIR_MOVIE']):
            os.makedirs(app.config['UP_DIR_MOVIE'])
            os.chmod('UP_DIR_MOVIE', "rw")  # 给目录授予读写权限
        url = change_filename(file_url)
        logo = change_filename(file_logo)
        # 保存到UP_DIR+改变后的文件夹名下
        form.url.data.save(app.config['UP_DIR_MOVIE'] + url)
        form.logo.data.save(app.config['UP_DIR_MOVIE'] + logo)
        # 添加到数据库
        movie = Movie(
            title = data.get('title'),
            url = url,  # 应用上传的方法
            info = data['info'],
            logo = logo,
            star = int(data['star']),
            playnum = 0,
            commentnum = 0,
            tag_id = int(data['tag_id']),
            area = data['area'],
            release_time = data['release_time'],
            length = data['length'],

        )
        db.session.add(movie)
        db.session.commit()
        flash('添加电影成功!', "ok")
        return redirect(url_for('admin.movie_add'))
    return render_template("admin/movie_add.html", form = form)


# 电影列表
@admin.route('/movie_list/<int:page>', methods = ['GET'])
@admin_login_req
def movie_list(page=None):
    if page == None:
        page = 1
    # 单表查询使用filter_by,多表查询使用filter关联
    page_data = Movie.query.join(Tag).filter(
        Tag.id == Movie.tag_id
    ).order_by(
        Movie.addtime.asc()
    ).paginate(page = page, per_page = 10)
    return render_template("admin/movie_list.html", page_data = page_data)


# 电影的删除
@admin.route('/movie_del/<int:id>', methods = ['GET'])
@admin_login_req

def movie_del(id=None):
    # movie = Movie.query.filter_by(id = int(id)).first_or_404()
    movie = Movie.query.get_or_404(int(id))
    db.session.delete(movie)
    db.session.commit()
    flash('删除电影成功', 'ok')
    return redirect(url_for('admin.movie_list', page = 1))


# 电影的编辑
@admin.route('/movie_edit/<int:id>', methods = ['GET', 'POST'])
@admin_login_req

def movie_edit(id=None):
    form = MovieForm()
    form.url.validators = []
    form.logo.validators = []
    movie = Movie.query.get_or_404(int(id))
    if request.method == "GET":
        form.info.data = movie.info
        form.tag_id.data = movie.tag_id
        form.star.data = movie.star
    # tag = Tag.query.get_or_404(id = id)
    if form.validate_on_submit():
        data = form.data
        movie_count = Movie.query.filter_by(title = data.get('title')).count()
        if movie_count == 1 and movie.title != data.get('title'):
            flash('片名已经存在!', "err")
            return redirect(url_for('admin.movie_edit', id = id))
        if not os.path.exists(app.config['UP_DIR_MOVIE']):
            os.makedirs(app.config['UP_DIR_MOVIE'])
            os.chmod('UP_DIR_MOVIE', 'rw')  # 给目录授予读写权限
        # 说明url不为空
        if form.url.data.filename != []:
            # 获取上传文件地址
            file_url = secure_filename(form.url.data.filename)
            movie.url = change_filename(file_url)
            # 保存到UP_DIR+改变后的文件夹名下
            form.url.data.save(app.config['UP_DIR_MOVIE'] + movie.url)
        if form.logo.data.filename != []:
            file_logo = secure_filename(form.logo.data.filename)
            movie.logo = change_filename(file_logo)
            form.logo.data.save(app.config['UP_DIR_MOVIE'] + movie.logo)
        # 添加到数据库
        movie.title = data.get('title')
        movie.info = data.get('info')
        movie.star = data.get('star')
        movie.tag_id = data.get('tag_id')
        movie.area = data.get('area')
        movie.length = data.get('length')
        movie.release_time = data.get("release_time")
        movie.addtime = data.get("addtime")
        db.session.add(movie)
        db.session.commit()
        flash('修改电影成功!', "ok")
        return redirect(url_for('admin.movie_edit', id = movie.id))
    return render_template("admin/movie_edit.html", form = form, movie = movie)


@admin.route('/moviecol_list/<int:page>', methods = ['GET'])
@admin_login_req

def moviecol_list(page=None):
    page_data = Moviecol.query.join(
        Movie
    ).join(
        User
    ).filter(
        Moviecol.movie_id == Movie.id,
        Moviecol.user_id == User.id
    ).order_by(
        Moviecol.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/moviecol_list.html", page_data = page_data)


# 删除电影收藏
@admin.route('/moviecol_del/<int:id>', methods = ['GET'])
@admin_login_req

def moviecol_del(id=None):
    moviecol = Moviecol.query.get_or_404(int(id))
    db.session.delete(moviecol)
    db.session.commit()
    flash('删除电影收藏成功', 'ok')
    return redirect(url_for('admin.moviecol_list', page = 1))


# 操作日志列表
@admin.route('/optlog_list/<int:page>', methods = ['GET'])
@admin_login_req

def optlog_list(page=None):
    if page == None:
        page = 1
    page_data = OptLog.query.join(
        Admin
    ).filter(
        OptLog.admin_id == Admin.id
    ).order_by(
        OptLog.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/optlog_list.html", page_data = page_data)


# 添加电影预告
@admin.route('/preview_add/', methods = ["GET", "POST"])
@admin_login_req

def preview_add():
    form = PreviewForm()
    if form.validate_on_submit():
        data = form.data
        # 获取封面地址
        file_logo = secure_filename(form.logo.data.filename)
        if not os.path.exists(app.config['UP_DIR_PREVIEW']):
            os.makedirs(app.config['UP_DIR_PREVIEW'])
            os.chmod('UP_DIR_PREVIEW', "rw")
        logo = change_filename(file_logo)
        form.logo.data.save(app.config['UP_DIR_PREVIEW'] + logo)
        preview = Preview(
            title = data.get('title'),
            logo = logo,
        )
        db.session.add(preview)
        db.session.commit()
        flash('添加电影预告成功', 'ok')
        return redirect(url_for('admin.preview_add'))
    return render_template("admin/preview_add.html", form = form)


# 预告列表
@admin.route('/preview_list/<int:page>', methods = ['GET'])
@admin_login_req

def preview_list(page=None):
    if page == None:
        page = 1
    # 单表查询使用filter_by,多表查询使用filter关联
    page_data = Preview.query.order_by(
        Preview.addtime.asc()
    ).paginate(page = page, per_page = 10)
    return render_template("admin/preview_list.html", page_data = page_data)


@admin.route('/preview_del/<int:id>', methods = ['GET'])
@admin_login_req

def preview_del(id=None):
    preview = Preview.query.filter_by(id = id).first_or_404()
    db.session.delete(preview)
    db.session.commit()
    flash('删除电影预告成功', 'ok')
    return redirect(url_for('admin.preview_list', page = 1))


@admin.route('/preview_edit/<int:id>', methods = ['GET', 'POST'])
@admin_login_req

def preview_edit(id=None):
    form = PreviewForm()
    form.logo.validators = []
    preview = Preview.query.filter_by(id = id).first_or_404()
    if request.method == 'GET':
        form.title.data = preview.title
    if form.validate_on_submit():
        data = form.data
        if form.logo.data.filename != '':
            file_logo = secure_filename(form.logo.data.filename)
            preview.logo = change_filename(file_logo)
            form.logo.data.save(app.config['UP_DIR_PREVIEW'] + preview.logo)
        # 添加到数据库
        preview.title = data.get('title')
        db.session.add(preview)
        db.session.commit()
        flash('修改电影预告成功!', "ok")
        return redirect(url_for('admin.preview_edit', id = preview.id))
    return render_template("admin/preview_edit.html", form = form, preview = preview)


# 添加角色
@admin.route('/role_add/', methods = ['GET', 'POST'])
@admin_login_req

def role_add():
    form = RoleForm()
    if form.validate_on_submit():
        data = form.data
        print(data.get('auths'))  # 列表形式

        role = Role(
            name = data.get('name'),
            auths = ','.join(map(lambda v: str(v), data.get('auths')))
        )
        db.session.add(role)
        db.session.commit()
        flash("角色添加成功", 'ok')
    return render_template("admin/role_add.html", form = form)


# 角色列表
@admin.route('/role_list/<int:page>', methods = ['GET'])
@admin_login_req

def role_list(page=None):
    if page == None:
        page = 1
    counter = 1
    page_data = Role.query.order_by(
        Role.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/role_list.html", page_data = page_data, counter = counter)


# 删除角色
@admin.route('/role_del/<int:id>', methods = ['GET'])
@admin_login_req

def role_del(id=None):
    role = Role.query.filter_by(id = id).first_or_404()
    db.session.delete(role)
    db.session.commit()
    flash('角色删除成功', 'ok')
    return redirect(url_for('admin.role_list', page = 1))


# 修改角色
@admin.route('/role_edit/<int:id>', methods = ['GET', 'POST'])
@admin_login_req

def role_edit(id=None):
    form = RoleForm()
    role = Role.query.get_or_404(int(id))
    if request.method == 'GET':
        auths = role.auths
        print(auths)
        form.auths.data = list(map(lambda v: int(v), auths.split(',')))
    if form.validate_on_submit():
        data = form.data
        role.name = data.get('name')

        db.session.add(role)
        db.session.commit()
        flash('角色修改成功', 'ok')
        return redirect(url_for('admin.role_edit', id = role.id))
    return render_template('admin/role_edit.html', form = form, role = role)


# 添加标签
@admin.route('/tag_add/', methods = ['GET', 'POST'])
@admin_login_req
@admin_auth
def tag_add():
    form = TagForm()
    if form.validate_on_submit():
        data = form.data
        print(data.get('name'))
        # 标签唯一性判断
        tag = Tag.query.filter_by(name = data.get('name')).count()
        if tag == 1:
            flash("标签已存在!", "err")
            return redirect(url_for('admin.tag_add'))
        # 添加到数据库
        tag = Tag(
            name = data.get('name')
        )
        db.session.add(tag)
        db.session.commit()
        flash('添加标签成功!', "ok")
        optlog = OptLog(
            admin_id = session['admin_id'],
            ip = request.remote_addr,
            reason = "添加标签%s" % data.get('name')
        )
        db.session.add(optlog)
        db.session.commit()
        return redirect(url_for('admin.tag_add'))
    return render_template("admin/tag_add.html", form = form)


# 标签列表
@admin.route('/tag_list/<int:page>/', methods = ['GET'])
@admin_login_req
@admin_auth
def tag_list(page=None):
    if page == None:
        page = 1
    page_data = Tag.query.order_by(
        Tag.addtime.asc()
    ).paginate(page = page, per_page = 10)
    return render_template("admin/tag_list.html", page_data = page_data)


# 标签的删除
@admin.route('/tag_del/<int:id>', methods = ['GET'])
@admin_login_req
@admin_auth
def tag_del(id=None):
    tag = Tag.query.filter_by(id = id).first_or_404()
    db.session.delete(tag)
    db.session.commit()
    flash('删除标签成功', 'ok')
    return redirect(url_for('admin.tag_list', page = 1))


# 标签的编辑
@admin.route('/tag_edit/<int:id>', methods = ['GET', 'POST'])
@admin_login_req
@admin_auth
def tag_edit(id=None):
    form = TagForm()
    tag = Tag.query.filter_by(id = id).first_or_404()
    # tag = Tag.query.get_or_404(id = id)
    if form.validate_on_submit():
        data = form.data
        # 标签唯一性判断
        tag_count = Tag.query.filter_by(name = data.get('name')).count()
        if tag.name != data.get('name') and tag_count == 1:
            flash("标签已存在!", "err")
            return redirect(url_for('admin.tag_edit', id = id))
        # 添加到数据库
        tag.name = data.get('name')
        db.session.add(tag)
        db.session.commit()
        flash('修改标签成功!', "ok")
        return redirect(url_for('admin.tag_edit', id = tag.id))
    return render_template("admin/tag_edit.html", form = form, tag = tag)


# 会员列表
@admin.route('/user_list/<int:page>', methods = ['GET'])
@admin_login_req

def user_list(page=None):
    if page == None:
        page = 1
    # 单表查询使用filter_by,多表查询使用filter关联
    page_data = User.query.order_by(
        User.addtime.desc()
    ).paginate(page = page, per_page = 10)
    return render_template("admin/user_list.html", page_data = page_data)


# 查看会员
@admin.route('/user_view/<int:id>', methods = ['GET'])
@admin_login_req

def user_view(id=None):
    user = User.query.get_or_404(int(id))
    return render_template("admin/user_view.html", user = user)


# 删除会员
@admin.route('/user_del/<int:id>', methods = ['GET'])
@admin_login_req
@admin_auth
def user_del(id=None):
    user = User.query.get_or_404(int(id))
    db.session.delete(user)
    db.session.commit()
    flash('会员删除成功', 'ok')
    return redirect(url_for('admin.user_list', page = 1))


@admin.route('/userloginlog_list/<int:page>', methods = ['GET'])
@admin_login_req

def userloginlog_list(page=None):
    if page == None:
        page = 1
    page_data = UserLog.query.join(
        User
    ).filter(
        UserLog.user_id == User.id
    ).order_by(
        UserLog.addtime.desc()
    ).paginate(page, per_page = 10)
    return render_template("admin/userloginlog_list.html", page_data = page_data)
