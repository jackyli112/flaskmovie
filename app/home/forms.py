#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:50
# 文件名称：forms.PY

from flask_wtf import FlaskForm
from wtforms.fields import SubmitField, StringField, PasswordField, FileField, TextAreaField, \
    SelectField, SelectMultipleField
from wtforms.validators import DataRequired, ValidationError, EqualTo,Email,Regexp
from app.models import Admin, User


class RegistForm(FlaskForm):
    name = StringField(
        label = "昵称",
        # 验证器
        validators = [
            DataRequired('请输入昵称！')
        ],
        description = "昵称",
        # 附加选项
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入昵称！",
        }
    )
    email = StringField(
        label = "邮箱",
        # 验证器
        validators = [
            DataRequired('请输入邮箱！'),
            Email('邮箱格式不正确！')
        ],
        description = "邮箱",
        # 附加选项
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入邮箱！",
        }
    )
    phone = StringField(
        label = "电话",
        # 验证器
        validators = [
            DataRequired('请输入电话！'),
            Regexp('1[3458]\\d{9}',message = '请输入正确的手机号码！')
        ],
        description = "电话",
        # 附加选项
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入电话！",
        }
    )
    pwd = PasswordField(
        label = "密码",
        # 验证器
        validators = [
            DataRequired('请输入密码！')
        ],
        description = "密码",
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入密码！",
            # 必须的选项
            # "required": "required"
        }
    )
    re_pwd = PasswordField(
        label = "确认密码",
        # 验证器
        validators = [
            DataRequired('请确认密码！'),
            EqualTo('pwd', message = "两次密码不一致")
        ],
        description = "确认密码",
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请确认密码！",
        }
    )
    submit = SubmitField(
        "注册",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-lg btn-success btn-block",
            # "required": "required"
        }
    )

    #验证模型字段唯一性

    def validata_name(self,field):
        name = field.data
        user = User.query.filter_by(name =name).count()
        if user ==1:
            raise  ValidationError('昵称已经存在!')


    def validata_email(self,field):
        email = field.data
        user = User.query.filter_by(email =email).count()
        if user ==1:
            raise  ValidationError('邮箱已经存在!')

    def validata_phone(self,field):
        phone = field.data
        user = User.query.filter_by(phone =phone).count()
        if user ==1:
            raise  ValidationError('手机号码已经存在!')


class LoginForm(FlaskForm):
    name = StringField(
        label = "账号",
        # 验证器
        validators = [
            DataRequired('请输入账号！')
        ],
        description = "账号",
        # 附加选项
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入账号！",
        }
    )
    pwd = PasswordField(
        label = "密码",
        # 验证器
        validators = [
            DataRequired('请输入密码！')
        ],
        description = "密码",
        render_kw = {
            "class": "form-control input-lg",
            "placeholder": "请输入密码！",
            # 必须的选项
            # "required": "required"
        }
    )
    submit = SubmitField(
        "登录",
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-lg btn-primary btn-block",
            # "required": "required"
        }
    )


#会员修改资料表单
class UserDetailForm(FlaskForm):
    name = StringField(
        label = '昵称',
        # 验证器
        validators = [
            DataRequired('请输入昵称！')
        ],
        description = "昵称",
        # 附加选项
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入昵称！",
        }
    )
    email = StringField(
        label = '邮箱',
        # 验证器
        validators = [
            DataRequired('请输入邮箱！'),
            Email('邮箱格式不正确！')
        ],
        description = "邮箱",
        # 附加选项
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入邮箱！",
        }
    )
    phone = StringField(
        label = '手机号码',
        # 验证器
        validators = [
            DataRequired('请输入手机号码！'),
            Regexp('1[3458]\\d{9}', message = '请输入正确的手机号码！')
        ],
        description = "手机号码",
        # 附加选项
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入电话！",
        }
    )
    face = FileField(
        label = '头像',
        validators = [
            DataRequired('请上传头像')
        ],
        description = '头像',
    )
    info  = TextAreaField(
        label = '简介',
        validators = [
            DataRequired('请输入简介'),
        ],
        description = '简介',
        render_kw = {
            "class": "form-control",
        }
    )
    submit = SubmitField(
        '保存修改',
        render_kw = {
            # "id":"btn-sub",
            "class": "btn btn-success",
            # "required": "required"
        }
    )


#密码
class PwdForm(FlaskForm):
    old_pwd = PasswordField(
        label = '旧密码',
        validators = [
            DataRequired('请输入旧密码！')
        ],
        description = "旧密码",
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入旧密码！"
        }
    )
    new_pwd = PasswordField(
        label = '新密码',
        validators = [
            DataRequired('请输入新密码！')
        ],
        description = "新密码",
        render_kw = {
            "class": "form-control",
            "placeholder": "请输入新密码！"
        }
    )
    submit = SubmitField(
        '修改密码',
        render_kw = {
            "class": "btn btn-success"
        }
    )

    def validate_old_pwd(self,field):
        from flask import session
        pwd = field.data
        name = session.get('user')
        user = User.query.filter_by(
            name = name
        ).first()
        if not user.check_pwd(pwd):
            raise ValidationError('旧密码错误！')



class CommentForm(FlaskForm):
    content = TextAreaField(
        label = '内容',
        validators = [
            DataRequired('请输入内容')
        ],
        description = '内容',
        render_kw = {
            "id": "input_content"
        }

    )
    submit = SubmitField(
        '提交评论',
        render_kw = {
            "class": "btn btn-success",
            "id":"btn-sub"
        }
    )