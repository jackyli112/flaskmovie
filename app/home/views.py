#!/usr/bin/env python 
# encoding: utf-8 
# 开发人员：li
# 开发团队：xiaoli
# 开发时间：2020/3/31 10:50
# 文件名称：views.PY
import datetime
import os
from functools import wraps
from werkzeug.utils import secure_filename
from app import db, app
from app.home.forms import RegistForm, LoginForm, UserDetailForm, PwdForm,CommentForm
from app.models import User, UserLog, Preview, Tag, Movie, Moviecol, Comment
from . import home
from flask import render_template, redirect, url_for, flash,request,session
from werkzeug.security import generate_password_hash
import uuid





# 访问限制处理器
def home_login_req(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # 如果session中没有admin
        if 'user' not in session:
            return redirect(url_for('home.login', next = request.url))
        return f(*args, **kwargs)
    return decorated_function

# 改变文件名称
def change_filename(filename):
    fileinfo = os.path.splitext(filename)  # 分割成后缀名加前面标题
    filename = datetime.date.today().strftime("%Y-%m-%d") + str(uuid.uuid4().hex) + fileinfo[-1]
    return filename


# 电影界面进行标签筛选
#从上面可以看出：
#当get请求时，需要使用request.args来获取数据
#当post请求时，需要使用request.form来获取数据
@home.route('/<int:page>/',methods = ['GET'])
@home_login_req
def index(page = None):
    tags= Tag.query.all()
    page_data = Movie.query
    #标签
    tid = request.args.get('tid', 0)
    if int(tid) !=0:
        page_data = page_data.filter_by(tag_id =int(tid))
    star = request.args.get('star',0)
    #星级
    if int(star) !=0:
        page_data = page_data.filter_by(star = int(star))
    time = request.args.get('time',0)
    #上映时间
    if int(time) != 0:
        if int(time) ==1:
            page_data = page_data.order_by(Movie.addtime.desc())
        else:
            page_data = page_data.order_by(Movie.addtime.asc())
    #播放量
    playnum = request.args.get('playnum',0)
    if int(playnum) != 0:
        if int(playnum) ==1:
            page_data = page_data.order_by(Movie.playnum.desc())
        else:
            page_data = page_data.order_by(Movie.playnum.asc())
    #评论数量
    cm = request.args.get('cm',0)
    if int(cm) != 0:
        if int(cm) == 1:
            page_data = page_data.order_by(Movie.commentnum.desc())
        else:
            page_data = page_data.order_by(Movie.commentnum.asc())
    if page == None:
        page =1
    page_data = page_data.paginate(page = page,per_page = 10)
    p = dict(
        tid = tid,
        time = time,
        star = star,
        playnum = playnum,
        cm = cm,
    )
    return render_template('home/index.html',tags = tags,p = p,page_data=page_data)


# 登录
@home.route('/login/',methods = ['GET','POST'])
def login():
    form  =  LoginForm()
    if form.validate_on_submit():
        data = form.data
        user = User.query.filter_by(name = data['name']).first()
        if not user.check_pwd(data.get('pwd')):
            flash('账号或密码错误','err')
            return redirect(url_for('home.login'))
        session['user'] = user.name
        session['user_id'] = user.id
        userlog = UserLog(
            user_id = user.id,
            ip = request.remote_addr,
        )
        db.session.add(userlog)
        db.session.commit()
        return redirect(url_for('home.user'))
    return render_template('/home/login.html',form = form)


# 退出
@home.route('/logout/')
def logout():
    session.pop('user',None)
    session.pop('user_id',None)
    return redirect(url_for('home.login'))


# 注册
@home.route('/register/', methods = ['GET', 'POST'])
def register():
    form = RegistForm()
    if form.validate_on_submit():
        data = form.data
        user = User(
            name = data.get('name'),
            pwd = generate_password_hash(data.get('pwd')),
            email = data.get('email'),
            phone = data.get('phone'),
            uuid = uuid.uuid4().hex
        )
        db.session.add(user)
        db.session.commit()
        flash('注册成功','ok')
        return redirect(url_for('home.login'))
    return render_template('/home/register.html', form = form)


# 会员中心
@home.route('/users/',methods = ['GET', 'POST'])
@home_login_req
def user():
    form = UserDetailForm()
    user = User.query.get_or_404(int(session['user_id'] ))
    #第一次上传头像 初始化
    form.face.validators = []
    if request.method == 'GET':
        form.name.data = user.name
        form.email.data = user.email
        form.phone.data = user.phone
        form.info.data = user.info
    if form.validate_on_submit():
        data = form.data
        # 前端记得加上enctype="multipart/form-data"
        file_face = secure_filename(form.face.data.filename)
        if not os.path.exists(app.config['UP_DIR_USER']):
            os.makedirs(app.config['UP_DIR_USER'])
            os.chmod('UP_DIR_USER', 'rw')  # 给目录授予读写权限
        user.face = change_filename(file_face)
        form.face.data.save(app.config['UP_DIR_USER'] + user.face)
        name_count = User.query.filter_by(name = data['name']).count()
        if data['name'] !=user.name and name_count == 1:
            flash('昵称已经存在','err')
            return redirect(url_for('home.user'))
        user.name = data.get('name')
        email_count = User.query.filter_by(email = data['email']).count()
        if data['email'] != user.email and email_count == 1:
            flash('邮箱已经存在', 'err')
            return redirect(url_for('home.user'))
        user.email = data.get('email')
        user.phone = data.get('phone')
        user.info = data.get('info')
        db.session.add(user)
        db.session.commit()
        flash("会员资料修改成功",'ok')
        return redirect(url_for('home.user'))
    return render_template('/home/user.html',form = form ,user =user)


# 修改密码
@home.route('/pwd/', methods = ['GET', 'POST'])
@home_login_req
def pwd():
    form = PwdForm()
    if form.validate_on_submit():
        data = form.data
        user = User.query.filter_by(name = session['user']).first()
        # if not user.check_pwd(data.get('old_pwd')):
        #     flash('密码输入错误,请重新输入！','err')
        #     return redirect(url_for('home.pwd'))
        from werkzeug.security import generate_password_hash
        user.pwd = generate_password_hash(data.get('new_pwd'))
        db.session.add(user)
        db.session.commit()
        flash('密码修改成功,请重新登录','ok')
        return redirect(url_for('home.logout'))
    return render_template('/home/pwd.html',form = form)


# 评论页面
@home.route('/comments/<int:page>/')
@home_login_req
def comments(page=None):
    if page == None:
        page =1
    page_data = Comment.query.join(
        Movie
    ).join(
        User
    ).filter(
        Movie.id == Comment.movie_id,
        User.id == session['user_id']
    ).order_by(
        Comment.addtime.desc()
    ).paginate(page=page,per_page = 10)
    return render_template('/home/comments.html',page_data=page_data)


# 电影收藏
@home.route('/moviecol/')
@home_login_req
def moviecol():
    return render_template('/home/moviecol.html')


# 会员登录日志
@home.route('/loginlog/<int:page>',methods = ['GET'])
@home_login_req
def loginlog(page = None):
    if page == None:
        page = 1
    page_data = UserLog.query.join(
        User
    ).filter(
        UserLog.user_id == User.id
    ).order_by(
        UserLog.addtime.desc()
    ).paginate(page,per_page = 10)
    return render_template('/home/loginlog.html',page_data = page_data)


# 动画 preview
@home.route('/animation/')
@home_login_req
def animation():
    data  = Preview.query.all()
    return render_template('/home/animation.html',data = data)


# 搜索
@home.route('/search/<int:page>')
@home_login_req
def search(page=None):
    if page == None:
        page = 1
    key = request.args.get('key','')
    movie_count = Movie.query.filter(
        Movie.title.ilike('key')
    ).count()
    page_data = Movie.query.filter(
        #模糊匹配
        Movie.title.ilike('%'+key+'%')
    ).order_by(
        Movie.addtime.desc()
    ).paginate(page=page,per_page = 10)
    return render_template('/home/search.html',key=key,page_data=page_data,movie_count=movie_count)


# 播放
@home.route('/play/<int:id>/<int:page>',methods=['GET','POST'])
@home_login_req
def play(id=None,page = None):
    movie = Movie.query.join(
        Tag
    ).filter(
        Movie.tag_id == Tag.id,
        Movie.id==int(id)
    ).first_or_404()
    if page == None:
        page =1
    page_data = Comment.query.join(
        Movie
    ).join(
        User
    ).filter(
        Movie.id == movie.id,
        User.id == session['user_id']
    ).order_by(
        Comment.addtime.desc()
    ).paginate(page=page,per_page = 10)
    movie.playnum = movie.playnum + 1
    form = CommentForm()
    if form.validate_on_submit() and 'user'in session:
        data = form.data
        comment = Comment(
            content = data.get('content'),
            movie_id = movie.id,
            user_id = Comment.user_id
        )
        db.session.add(comment)
        db.session.commit()
        movie.commentnum = movie.commentnum + 1
        flash('评论添加成功', 'ok')
        db.session.add(movie)
        db.session.commit()
        return redirect(url_for('home.play',id = movie.id,page = 1))
    db.session.add(movie)
    db.session.commit()
    return render_template('/home/play.html',movie = movie,form = form,page_data=page_data)
